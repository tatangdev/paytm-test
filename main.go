package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
)

func main() {
	jsonFile, err := os.Open("order.json")
	if err != nil {
		fmt.Println(err)
	}
	defer jsonFile.Close()

	byteValue, _ := ioutil.ReadAll(jsonFile)

	var order Order
	json.Unmarshal(byteValue, &order)

	paytmHost := "https://securegw-stage.paytm.in"
	paytmMid := "lcPqRy21595458128685"
	paytmMerchatKey := "ru37%Mqjm#9%TfWv"
	website := "WEBSTAGING"

	orderId := fmt.Sprintf("RUSHOWL_%d", order.OrderId)
	callbackUrl := fmt.Sprintf("https://eooddia4ek39hgp.m.pipedream.net/?order_id=%s", orderId)
	userInfo := map[string]interface{}{
		"custId": "CUST_001",
	}
	txnAmount := map[string]interface{}{
		"value":    "15.00",
		"currency": "INR",
	}

	paytmParams := map[string]interface{}{
		"requestType": "Payment",
		"mid":         paytmMid,
		"orderId":     orderId,
		"websiteName": website,
		"userInfo":    userInfo,
		"txnAmount":   txnAmount,
		"callbackUrl": callbackUrl,
	}

	b, err := json.Marshal(&paytmParams)
	if err != nil {
		fmt.Println(err)
		return
	}

	paytmChecksum := GenerateSignatureByString(string(b), paytmMerchatKey)

	body := map[string]interface{}{
		"body": paytmParams,
		"head": map[string]interface{}{
			"signature": paytmChecksum,
		},
	}

	b, err = json.Marshal(&body)
	if err != nil {
		fmt.Println(err)
		return
	}

	url := fmt.Sprintf("%s/theia/api/v1/initiateTransaction?mid=%s&orderId=%s", paytmHost, paytmMid, orderId)
	httpRes, err := http.Post(url, "application/json", bytes.NewBuffer(b))
	if err != nil {
		fmt.Println(err)
		return
	}

	responseBody, err := ioutil.ReadAll(httpRes.Body)
	if err != nil {
		fmt.Println(err)
		return
	}

	responseData := &paytmInitiateTransaction{}
	err = json.Unmarshal(responseBody, &responseData)
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println("txn_token :", responseData.Body.TxnToken)
	fmt.Println("order_id :", orderId)
	fmt.Println("callback_url :", callbackUrl)

	// update jsonData
	order.OrderId = order.OrderId + 1
	file, _ := json.MarshalIndent(order, "", " ")
	_ = ioutil.WriteFile("order.json", file, 0644)
}

type Order struct {
	OrderId int64 `json:"order_id"`
}

type paytmInitiateTransaction struct {
	Head *paytmInitiateTransactionBody `json:"head"`
	Body *paytmInitiateTransactionBody `json:"body"`
}

type paytmInitiateTransactionBody struct {
	ResultInfo       *paytmInitiateTransactionResultInfo `json:"resultInfo"`
	TxnToken         string                              `json:"txnToken"`
	IsPromoCodeValid bool                                `json:"isPromoCodeValid"`
	Authenticated    bool                                `json:"authenticated"`
	ExtraParamsMap   map[string]interface{}              `json:"extraParamsMap"`
}

type paytmInitiateTransactionResultInfo struct {
	ResultStatus string `json:"resultStatus"`
	ResultCode   string `json:"resultCode"`
	ResultMsg    string `json:"resultMsg"`
	IsRedirect   bool   `json:"isRedirect"`
	BankRetry    bool   `json:"bankRetry"`
	Retry        bool   `json:"retry"`
}

type paytmInitiateTransactionHead struct {
	ResponseTimestamp string `json:"responseTimestamp"`
	Version           string `json:"version"`
	ClientId          string `json:"clientId"`
	Signature         string `json:"signature"`
}
